const MemDown = require('memdown');
const { clone, uuid } = require('@m-ld/m-ld');
const { MqttRemotes } = require('@m-ld/m-ld/dist/mqtt');
//const { IoRemotes } = require('@m-ld/m-ld/dist/socket.io');
const config = {
  '@id': uuid(),
  '@domain': 'test.example.org',
  genesis: true,

  //@Christoph: Adapt host
  mqtt: { host: 'localhost', port: 1883 }
 
};

(async function () {
  const meld1 = await clone(new MemDown, MqttRemotes, config)
  //const meld1 = await clone(new MemDown, IoRemotes, config)
  
  meld1.status.subscribe(status => console.log('clone 1', 'status is', status))


  // ...
})();

