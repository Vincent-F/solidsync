const { fork } = require('child_process');

function started(childProcess) {
  return new Promise(resolve => childProcess.on('message', message => {
    if (message === 'started')
      resolve()
  }));
}

(async function () {
  var a=performance.now
  await started(fork('MQTT.js'))
  //await started(fork('socket.js'))
  await started(fork('clone.js'))
  var b=performance.now
  console.log("setup Genesis clone", b-a)
})();